/* GULP INIT */
var gulp            = require('gulp');
var plumber         = require('gulp-plumber');
var sass            = require('gulp-sass');
var watch           = require('gulp-watch');
var cssnano         = require('gulp-cssnano');
var rename          = require('gulp-rename');
var concat          = require('gulp-concat');
var uglify          = require('gulp-uglify');
var merge2          = require('merge2');
var imagemin        = require('gulp-imagemin');
var ignore          = require('gulp-ignore');
var rimraf          = require('gulp-rimraf');
var clone           = require('gulp-clone');
var merge           = require('gulp-merge');
var sourcemaps      = require('gulp-sourcemaps');
var del             = require('del');
var cleanCSS        = require('gulp-clean-css');
var gulpSequence    = require('gulp-sequence');
var ncu             = require('npm-check-updates');
var browserSync     = require('browser-sync').create();
var reload          = browserSync.reload;
// ===================

/* CHECK NPM UDPATES */
gulp.task('check-updates', function() {
    ncu.run({
        
        packageFile: 'package.json',
        silent: true,
        jsonUpgraded: true
    
        }).then((upgraded) => {
            console.log('dependencies to upgrade:', upgraded);
    });
});
// ====================

/* BASE PATH */
var basePaths = {
    node:   './node_modules/',
    assets: './assets/',
    dist:   './dist/'
};
// ====================

/* BROWSER SYNC */
// info: https://www.browsersync.io/docs/options/
var browserSyncOptions = {
    proxy: "barvyatakylaky.local",
    notify: true,
    open:   true
};

gulp.task('browser-sync', function() {
    browserSync.init(browserSyncOptions);
});
// =====================

/* STYLES */
gulp.task('sass', function () {
    gulp.src( basePaths.assets + '/scss/theme.scss' )
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass())
        .pipe(gulp.dest( basePaths.dist +'./css' ));
});

gulp.task('cssnano', function(){
    return gulp.src( basePaths.dist + './css/theme.css' )
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(plumber({
              errorHandler: function (err) {
                  console.log(err);
                  this.emit('end');
              }
          }))
      .pipe(rename({suffix: '.min'}))
      .pipe(cssnano({discardComments: {removeAll: true}}))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest( basePaths.dist + './css/' ))
  });
  
  gulp.task('minify-css', function() {
    return gulp.src( basePaths.dist + './css/theme.css' )
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(cleanCSS({compatibility: '*'}))
      .pipe(plumber({
              errorHandler: function (err) {
                  console.log(err);
                  this.emit('end');
              }
          }))
      .pipe(rename({suffix: '.min'}))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest( basePaths.dist +  './css/' ))
      .pipe(browserSync.stream());
  });
  
  gulp.task('styles', function(callback){ gulpSequence('sass', 'cssnano', 'minify-css')(callback) });
// =====================

/* SCRIPTS */
gulp.task('scripts', function() {
    gulp.src( basePaths.assets + 'js/**/*.js' )
      .pipe(concat('theme.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(basePaths.dist + './js/'));
  
    gulp.src( basePaths.assets + 'js/**/*.js' )
      .pipe(concat('theme.js'))
      .pipe(gulp.dest(basePaths.dist + './js/'));
  });
// =====================

/* IMAGE OPTIMIZING */
gulp.task('imagemin', function(){
    gulp.src( basePaths.assets + 'img/src/**' )
    .pipe(imagemin())
    .pipe(gulp.dest(basePaths.dist + 'img'))
});
// =====================

/* WATCHS */
// Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
gulp.task('watch', ['browser-sync', 'styles', 'scripts', 'imagemin'], function () {

    gulp.watch([ basePaths.assets + 'js/**/*.js', '!js/theme.js','!js/theme.min.js'], ['scripts']);
    gulp.watch( basePaths.assets + 'img/**', ['imagemin'] );
    gulp.watch( basePaths.assets + 'scss/**/*.scss', ['styles'] );
    gulp.watch( './**/*.php' ).on("change", reload);

 });

