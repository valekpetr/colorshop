<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bulmapress
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="is-fullheight">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Lato:400,700&subset=latin-ext');
	</style>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
			<div class="preheader navbar is-primary" role="navigation" aria-label="Pre header">
				<div class="container preheader__content">
					<div class="navbar-start is-hidden-touch">
						<div class="preheader__welcome-msg">
							<div class="navbar-item">
								<p>
									<?php echo get_theme_mod('welcome_message', 'Welcome in our e-shop!'); ?>
								</p>
							</div>
					    </div>
					</div>
					<div class="navbar-end">
						<nav class="preheader__secondary-nav is-uppercase " role="navigation" aria-label="secondary navigation">
							<?php secondary_navigation(); ?>
						</nav>
						<div class="preheader__user-info is-uppercase ">
							
						<?php if (!is_user_logged_in()) : ?>
							
							<div class="navbar-menu">
								<div class="navbar-item">
								<a title="Přihlásit se" class="navbar-item" href="#" data-modal-login>
									<span class="icon"><i class="oi" data-glyph="person"></i></span>Přihlásit
								</a>
								</div>
							</div>

						<?php else:
								if (is_user_logged_in()) {
									user_navigation(); 
								}
							endif; ?>

						</div>
					</div>
				</div>
			</div>
			<header id="header" class="header navbar is-primary">
				<div class="container">
					<div class="header__logo navbar-brand navbar-start">
						<?php if ( function_exists('the_custom_logo')) {
							the_custom_logo();
						} else {
							bulmapress_home_link('navbar-item is-brand'); 
						} ?>

					</div>
					<div class="header__search">
						<?php echo do_shortcode( '[aws_search_form]' ); ?>
					</div>
					<div class="header__cart navbar-end dropdown is-hoverable">
						<div class="header__cart-content  dropdown-trigger">
							<span class="header__cart-icon icon"><span class="oi" data-glyph="cart"></span></span>
							<span class="header__cart-counter is-size-7">
								<?php echo WC()->cart->get_cart_contents_count(); ?>
							</span>
							<span class="header__cart-price is-size-6">	<?php echo WC()->cart->get_cart_total(); ?></span>
						</div>
						<?php header_cart_dropdown(); ?>
						</div>
					</div>
				</div>
			</header>
			<nav id="site-navigation" class="navbar main-navigation is-white" role="navigation" aria-label="main navigation" >
					<div class="container">

						<?php bulmapress_menu_toggle(); ?>

						<?php bulmapress_navigation(); ?>

				</div>
			</nav>
		<!-- #site-navigation -->	

		<div id="content" class="site-content">
	