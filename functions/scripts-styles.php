<?php
/**
 * Scripts & Styles
 *
 * @package Bulmapress
 */

/**
 * Enqueue scripts and styles.
 */
function bulmapress_scripts() {
	wp_enqueue_style( 'bulmapress-style', get_stylesheet_uri() );

	wp_enqueue_style( 'stylesheet', get_template_directory_uri() . '/dist/css/theme.min.css' );

	//wp_enqueue_script( 'scripts', get_template_directory_uri() . '/dist/js/theme.min.js', array(), '20151215', true );

	wp_enqueue_script( 'scripts', get_template_directory_uri() .'/dist/js/theme.min.js', array('jquery'), null, true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bulmapress_scripts' );
