<?php
/**
 * Base Functions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Bulmapress
 */

/*
 * Make theme available for translation.
 * Translations can be filed in the /languages/ directory.
 * If you're building a theme based on Bulmapress, use a find and replace
 * to change 'bulmapress' to the name of your theme in all the template files.
 */
load_theme_textdomain( 'bulmapress', get_template_directory() . '/languages' );

// Add default posts and comments RSS feed links to head.
add_theme_support( 'automatic-feed-links' );

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );
add_theme_support( 'custom-logo', array(
	'height'      => 60,
	'flex-height' => TRUE,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );

/*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support( 'html5', array(
	'search-form',
	'comment-form',
	'comment-list',
	'gallery',
	'caption',
	) );

/*
* Custom image sizes
*/
add_image_size( 'cart-product-thumb', 40, 40);

/*
* WooCommerce
*/
add_theme_support( 'woocommerce' );
