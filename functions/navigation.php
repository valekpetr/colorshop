<?php
/**
 * Navigation Functions
 *
 * @package Bulmapress
 */

// This theme uses wp_nav_menu() in one location.
register_nav_menus( array(
	'menu-1' => esc_html__( 'Primary', 'bulmapress' ),
	'menu-2' => esc_html__( 'Secondary', 'bulmapress' ),
	'menu-3' => esc_html__( 'User', 'bulmapress' ),
	) );

// Primary navigation
function bulmapress_navigation() {
	wp_nav_menu( array(
		'theme_location'    => 'menu-1',
		'depth'             => 2,
		'container'         => 'div id="navigation"',
		'menu_class'        => 'navbar-menu',
		'fallback_cb'       => 'bulmapress_navwalker::fallback',
		'walker'            => new bulmapress_navwalker()
		)
	);
}
// Secondary navigation
function secondary_navigation() {
	wp_nav_menu( array(
		'theme_location'    => 'menu-2',
		'depth'             => 2,
		'container'         => 'div id="navigation"',
		'menu_class'        => 'navbar-menu',
		'fallback_cb'       => 'bulmapress_navwalker::fallback',
		'walker'            => new bulmapress_navwalker()
		)
	);
}
// User navigation
function user_navigation() {
	wp_nav_menu( array(
		'theme_location'    => 'menu-3',
		'depth'             => 2,
		'container'         => 'div id="navigation"',
		'menu_class'        => 'navbar-menu',
		'fallback_cb'       => 'bulmapress_navwalker::fallback',
		'walker'            => new bulmapress_navwalker()
		)
	);
}
