<?php
/**
 * Helper Functions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Bulmapress
 */

function bulmapress_home_link($class)
{
	$output = '<a href="' . esc_url( home_url( '/' ) ) . '" class="' . $class . '" rel="home">' . get_bloginfo('name') . '</a>';
	echo $output;
}


function bulmapress_blog_description($class)
{
	$description = get_bloginfo( 'description', 'display' );
	$output = '';
	if ( $description || is_customize_preview() ) {
		$output.= '<p class="'. $class .' site-description is-hidden-mobile">'. $description . '</p>';
	}
	echo $output;
}


function bulmapress_skip_link_screen_reader_text()
{
	$output = '<a class="skip-link screen-reader-text" href="#content">Skip to content</a>';
	echo $output;
}


function bulmapress_menu_toggle()
{
	$output = '
		<button id="menu-toggle" class="navbar-burger burger" aria-controls="primary-menu" aria-expanded="false">
				<span></span>
				<span></span>
				<span></span>
		</button>';
	echo $output;
}

function bulmapress_copyright_link($author = 'Bulmapress', $url = 'http://bulmapress.com')
{
	$output = '
		<p class="copyright-link">&copy; '. date('Y') . '
			<a href=" ' . $url . '">' . $author . '</a>
		</p>';
	echo $output;
}


function bulmapress_the_title($class = 'is-3', $link = TRUE)
{
	$heading = _bulmapress_convert_heading($class);
	$title_before = '<' . $heading . ' class="title entry-title ' . $class . '">';
	$title_after = '</' . $heading . '>';
	if ($link === TRUE) {
		$output = the_title( sprintf( $title_before .'<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' . $title_after );
	} else {
		$output = $title_before . get_the_title() . $title_after;
	}
	echo $output;
}

function _bulmapress_convert_heading($class)
{
	switch ($class) {
		case 'is-1':
		$heading = 'h1';
		break;

		case 'is-2':
		$heading = 'h2';
		break;

		case 'is-3':
		$heading = 'h3';
		break;

		case 'is-4':
		$heading = 'h4';
		break;

		case 'is-5':
		$heading = 'h5';
		break;

		case 'is-6':
		$heading = 'h6';
		break;

		default:
		$heading = 'h3';
		break;
	}
	return $heading;
}

function bulmapress_get_comments()
{
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}
}

function bulmapress_login_modal()
{
	$output = '
	<div class="modal modal-login">
	<div class="modal-background"></div>
		<div class="modal-content">
			'. do_shortcode( '[woocommerce_my_account]' ) .'
		</div>
	<button class="modal-close is-large" aria-label="close"></button>
	</div>';
	echo $output;
}

function header_cart_dropdown()
{	
	$output = '	<div class="dropdown-menu header__cart-dropdown" id="dropdown-menu4" role="menu">
	<div class="dropdown-content">';

	if(!WC()->cart->is_empty()) {
		
	$output .= '<table class="dropdown-item cart-dropdown__item ">';
	
	foreach ( WC()->cart->get_cart() as $cart_item ) {
			$item_name = $cart_item['data']->get_title();
			$quantity = $cart_item['quantity'];
			$price = $cart_item['data']->get_price();
			$image = $cart_item['data']->get_image('cart-product-thumb');

			$output .= '
			<tr>
				<th rowspan="2" class="product-qty">' .  $quantity . ' x</th>
				<th class="product-name"><h6 class="is-size-6 is-uppercase has-text-weight-bold">' .  $item_name . ' </h6</th>
				<th rowspan="2" class="product-image">' . $image . '</th>
			</tr>
			<tr>
				<td class="product-price"><span>' . $price . ' </span> <span>' . get_woocommerce_currency_symbol() . '  </span></td>
			</tr>';
		}
	
	$output .= '				
		</table>
		<div class="cart-dropdown__buttons">	
			<a href="'. WC()->cart->get_cart_url() . '" title="' . esc_html__( 'Go to shopping cart', 'bulmapress' ) . '">
				<button class="cart-dropdown__cart-btn button is-primary is-outlined has-text-weight-bold">
					' . esc_html__( 'Cart', 'bulmapress' ) . ' 
				</button>
			</a>
			<a href="'. WC()->cart->get_checkout_url() . '" title="' . esc_html__( 'Go to checkout', 'bulmapress' ) . '">
				<button class="cart-dropdown__checkout-btn button is-secondary has-text-weight-bold">
					' . esc_html__( 'Checkout', 'bulmapress' ) . ' <span class="icon oi" data-glyph="caret-right"></span>
				</button>
			</a>
		</div>';

	} else {
		$output .= '<div class="dropdown-item"><h6 class="is-size-6">' . esc_html__( 'Cart is empty.', 'bulmapress' ) . '</h6></div>';
	}
	$output .= '	</div>
	</div>';
	echo $output;
}