<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */

get_header(); ?>

<section id="primary" class="content-area section">
	<main id="main" class="site-main wrapper" role="main">
    <div class="container">
		<?php woocommerce_content(); ?>
    </div>
  </main><!-- #main -->
</section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
