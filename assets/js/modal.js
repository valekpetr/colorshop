//
// MODAL
// This function open modal window
// --------------------------------

(function($) {
    $(document).ready(function() {

        var $modal         = $('.modal'),
            $modalTrigger  = $('[data-modal-login]'),
            $modalClose    = $('.modal-close');
        
        $modalTrigger.on('click', function(){
            $modal.addClass('is-active');
        });
        $modalClose.on('click', function(){
            $modal.removeClass('is-active');
        });

        var $loginModal     = $('.modal-login'),
            $loginError     = $('.modal-login .woocommerce-error');

        if ($loginError.length){
            $loginModal.addClass('is-active');
        }
        
    });
})(jQuery);